﻿using System;

namespace ConsoleApplicationTask2
{
    class Program
    {
        static void Main(string[] args)
        {
         
            Console.WriteLine("Create 3 rectangles wiht next positions");
            Console.WriteLine("new Rectangle(35, 50, 55, 70)"+
            "\nnew Rectangle(20, 24, 70, 58)" +
                "\nnew Rectangle(110, 12, 126, 20)");
            Console.WriteLine("Show rectangles");
            var rectangle1 = new Rectangle(35, 50, 55, 70);
            var rectangle2 = new Rectangle(20, 24, 70,58);
            var rectangle3 = new Rectangle(110,12,126,20);

            Print(rectangle1);
            Print(rectangle2);
            Print(rectangle3);

            Console.WriteLine("\nCreate an intersecting rectangle rectangle1 and rectangle2");
            var recInter1To2 = Rectangle.CreateIntersectRectangle(rectangle1, rectangle2);
            Console.WriteLine("Rectangle.CreateIntersectRectangle(rectangle1, rectangle2)");
            Console.WriteLine("Show intersecting rectangle");
            Print(recInter1To2);

            Console.WriteLine("\nCreate an intersecting rectangle rectangle1 and rectangle3");
            var recInter1To3 = Rectangle.CreateIntersectRectangle(rectangle1, rectangle3);
            Console.WriteLine("Rectangle.CreateIntersectRectangle(rectangle1, rectangle3)");
            Console.WriteLine("Show intersecting rectangle");
            Print(recInter1To3);

            Console.WriteLine("\nCreate an common rectangle rectangle1 and rectangle2");
            var recCommon1To2 = Rectangle.CreateCommonRectangle(rectangle1, rectangle2);
            Console.WriteLine("Rectangle.CreateCommonRectangle(rectangle1, rectangle2)");
            Console.WriteLine("Show common rectangle");
            Print(recCommon1To2);

            Console.WriteLine("\nCreate an common rectangle rectangle1 and rectangle3");
            var recCommon1To3 = Rectangle.CreateCommonRectangle(rectangle1, rectangle3);
            Console.WriteLine("Rectangle.CreateCommonRectangle(rectangle1, rectangle3)");
            Console.WriteLine("Show common rectangle");
            Print(recCommon1To3);

            Console.WriteLine("\nMove rectangle1 to position 95,5");
            Console.WriteLine("rectangle1.MoveX(95,5)");
            rectangle1.MoveX(95,5);
            Console.WriteLine("Show rectangle1");
            Print(rectangle1);

            Console.WriteLine("\nChange rectangle1 ");
            Console.WriteLine("rectangle1.ChangeSize(5,-5)");
            rectangle1.ChangeSize(5,-5);
            Console.WriteLine("Show rectangle1");
            Print(rectangle1);

            Console.WriteLine("\nAnd again create an intersecting rectangle rectangle1 and rectangle3");
            recInter1To3 = Rectangle.CreateIntersectRectangle(rectangle1, rectangle3);
            Console.WriteLine("Rectangle.CreateIntersectRectangle(rectangle1, rectangle3)");
            Console.WriteLine("Show intersecting rectangle");
            Print(recInter1To3);

            Console.WriteLine("rectangle1.Height "+rectangle1.Height);
            Console.WriteLine("rectangle1.Width "+rectangle1.Width);
            Console.ReadKey();
        }

        static void Print(Rectangle rec)
        {
            if (rec==null)
            {
                Console.WriteLine("Rectangle does not exist");
                return;
            }
            Console.WriteLine(rec);
        }
    }
}
