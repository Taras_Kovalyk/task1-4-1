﻿using System;

namespace ConsoleApplicationTask2
{
    /// <summary>
    /// Represents a Rectangle class
    /// </summary>
    public class Rectangle
    {
        private Point _bottomLeft;
        private Point _upperRight;
        private int _height;
        private int _width;
        private static readonly Rectangle Empty;

        /// <summary>
        /// Gets the height of rectangle
        /// </summary>
        public int Height
        {
            get { return GetDistanceY(); }
        }

        /// <summary>
        ///  Gets the width of rectangle
        /// </summary>
        public int Width
        {
            get { return GetDistanceX(); }
        }

        /// <summary>
        /// Gets the x-coordinate and y-coordinate of the bottom left edge
        /// </summary>
        public Point BottomLeft
        {
            get { return _bottomLeft; }
        }
        /// <summary>
        /// Gets the x-coordinate and y-coordinate of the upper right edge
        /// </summary>
        public Point UpperRight
        {
            get { return _upperRight; }
        }
        /// <summary>
        /// Initializes a new instance of the Rectangle class with the specified location
        /// </summary>
        /// <param name="x">The x-coordinate of the bottom-left corner of the rectangle.</param>
        /// <param name="y">The y-coordinate of the bottom-left corner of the rectangle.</param>
        /// <param name="x2">The y-coordinate of the upper-right corner of the rectangle.</param>
        /// <param name="y2">The y-coordinate of the upper-right corner of the rectangle.</param>
        public Rectangle(int x, int y, int x2, int y2)
        {
            if (x2 <= x || y2 <= y)
            {
                throw new ArgumentException(
                    "The coordinates of the second point must be greater than the coordinates of the first point");
            }
            _bottomLeft = new Point(x, y);
            _upperRight = new Point(x2, y2);
            GetDistanceX();
            GetDistanceY();
        }

        private int GetDistanceX()
        {
            _width = _upperRight.X - _bottomLeft.X;
            return _width;
        }

        private int GetDistanceY()
        {
            _height = _upperRight.Y - _bottomLeft.Y;
            return _height;
        }
        /// <summary>
        /// Moves a rectangle to a new position with the coordinates x, y
        /// </summary>
        /// <param name="x">The x-coordinate of the bottom-left corner of the rectangle.</param>
        /// <param name="y">The y-coordinate of the bottom-left corner of the rectangle.</param>
        public void MoveX(int x, int y)
        {
            _bottomLeft.X = x;
            _bottomLeft.Y = y;
            _upperRight.X = x + _width;
            _upperRight.Y = y + _height;
        }
        /// <summary>
        /// Increases or decreases the size depending on the sign of the parameter
        /// </summary>
        /// <param name="x">Parameter by x</param>
        /// <param name="y">Parameter by y</param>
        public void ChangeSize(int x, int y)
        {
            _upperRight.X += x;
            _upperRight.Y += y;
        }
        /// <summary>
        /// Returns a third rectangle that represents the common rectangle of two other 
        /// </summary>
        /// <param name="rectangle1">Rectangle for generalization</param>
        /// <param name="rectangle2">Rectangle for generalization</param>
        /// <returns>A rectangle that represents the generalization</returns>
        public static Rectangle CreateCommonRectangle(Rectangle rectangle1, Rectangle rectangle2)
        {
            if (rectangle1==null || rectangle2==null)
            {
                throw new NullReferenceException();
            }
            var commonRectangle = new Rectangle(0,0,1,1);
            commonRectangle._bottomLeft.X = rectangle1._bottomLeft.X <= rectangle2.BottomLeft.X
                ? rectangle1._bottomLeft.X
                :rectangle2._bottomLeft.X;
            commonRectangle._bottomLeft.Y = rectangle1._bottomLeft.Y <= rectangle2._bottomLeft.Y
                ? rectangle1._bottomLeft.Y
                : rectangle2._bottomLeft.Y;

            commonRectangle._upperRight.X = rectangle1._upperRight.X <= rectangle2._upperRight.X
               ? rectangle2._upperRight.X
               : rectangle1._upperRight.X;
            commonRectangle._upperRight.Y = rectangle1._upperRight.Y <= rectangle2._upperRight.Y
                ? rectangle2._upperRight.Y
                : rectangle1._upperRight.Y;

           return commonRectangle;
        }
        /// <summary>
        /// Returns a third rectangle that represents the intersection of two other. If there is no intersection, an empty 
        /// </summary>
        /// <param name="rectangle1">A rectangle to intersect.</param>
        /// <param name="rectangle2">A rectangle to intersect.</param>
        /// <returns>A rectangle that represents the intersection</returns>
        public static Rectangle CreateIntersectRectangle(Rectangle rectangle1, Rectangle rectangle2)
        {
            if (rectangle1 == null || rectangle2 == null)
            {
                throw new NullReferenceException();
            }
            
            int leftX = Math.Max(rectangle1._bottomLeft.X, rectangle2._bottomLeft.X);
            int rightX = Math.Min(rectangle1._upperRight.X, rectangle2.UpperRight.X);

            int bottomY = Math.Max(rectangle1.BottomLeft.Y, rectangle2.BottomLeft.Y);
            int upperY = Math.Min(rectangle1.UpperRight.Y, rectangle2._upperRight.Y);

            if (leftX > rightX || bottomY > upperY)
            {
                return Empty;
            }

            return new Rectangle(leftX,bottomY,rightX,upperY);
        }
        /// <summary>
        /// Converts the attributes of this rectangle to a human-readable string.
        /// </summary>
        /// <returns>A string that contains positions bottom left edge and upper right edge</returns>
        public override string ToString()
        {
            return string.Format("Bottom left  X={0},Y={1}. Upper Right  X={2},Y={3}", _bottomLeft.X, _bottomLeft.Y,
                _upperRight.X, _upperRight.Y);
        }
    }
}

public struct Point
{
    private int _x;
    private int _y;

    public int X
    {
        get { return _x; }
        set { _x = value; }
    }

    public int Y
    {
        get { return _y; }
        set { _y = value; }
    }
    public Point(int x, int y)
    {
        _x = x;
        _y = y;
    }
}
