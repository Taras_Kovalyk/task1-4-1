﻿using System;
using System.Collections.Generic;

namespace ConsoleApplicationTask4
{
    /// <summary>
    /// A class representing a matrix
    /// </summary>
    public class Matrix
    {
        private int[,] _matrix;
        private Matrix _lastMinor;
        private Matrix _lastMinorK;
        private int _column;
        private int _row;

        /// <summary>
        /// Initializes a new instance of the Matrix class
        /// </summary>
        /// <param name="column">The number of columns</param>
        /// <param name="row">The number of rows</param>
        public Matrix(int column, int row)
        {
            _matrix = new int[column, row];
            _column = column;
            _row = row;
        }

        /// <summary>
        /// Gets the minor
        /// </summary>
        public Matrix LastMinor
        {
            get { return _lastMinor; }
        }
        /// <summary>
        /// Get the minor K-th order
        /// </summary>
        public Matrix LastMinorK
        {
            get { return _lastMinorK; }
        }
        /// <summary>
        /// Get the naumber of columns
        /// </summary>
        public int Column
        {
            get { return _column; }
        }
        /// <summary>
        /// Get the naumber of rows
        /// </summary>
        public int Row
        {
            get { return _row; }
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="m">The index of column</param>
        /// <param name="n">The index of row</param>
        /// <returns>Value at the specified index.</returns>
        public int this[int m, int n]
        {
            get { return _matrix[m, n]; }
            set { _matrix[m, n] = value; }
        }
        /// <summary>
        /// Fills the matrix with elements
        /// </summary>
        /// <param name="param">All elements of the matrix line by line</param>
        public void FillMatrix(params int[] param)
        {
            int index = 0;
            if (param.Length > _matrix.Length || param.Length == 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            for (int i = 0; i < _row; i++)
            {
                for (int j = 0; j < _column; j++)
                {
                    if (index < param.Length)
                    {
                        _matrix[j, i] = param[index];
                        index++;
                    }
                }
            }
        }
        /// <summary>
        /// Multiplies two matrices
        /// </summary>
        /// <param name="matrix1">First matrix</param>
        /// <param name="matrix2">Second matrix</param>
        /// <returns>The matrix obtained as a result of multiplication</returns>
        public static Matrix Мultiply(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1._column != matrix2._row)
            {
                throw new ArgumentException(
                    "The number of columns of the first matrix should be equal to the number of rows of the second matrix");
            }

            var multiply = new Matrix(matrix2._column, matrix1._row);
            for (int i = 0; i < matrix1._row; i++)
            {
                for (int j = 0; j < matrix2._column; j++)
                {
                    for (int k = 0; k < matrix1._column; k++)
                    {
                        multiply[j, i] += matrix1[k, i]*matrix2[j, k];
                    }
                }
            }
            return multiply;
        }
        /// <summary>
        /// Adds two matrices
        /// </summary>
        /// <param name="matrix1">First matrix</param>
        /// <param name="matrix2">Second matrix</param>
        /// <returns>The matrix obtained as a result of addition</returns>
        public static Matrix Add(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1._column != matrix2._column || matrix1._row != matrix2._row)
            {
                throw new ArgumentException("Matrices of different sizes");
            }

            var addMatrix = new Matrix(matrix1._column, matrix1._row);

            for (int i = 0; i < matrix1._row; i++)
            {
                for (int j = 0; j < matrix1._column; j++)
                {
                    addMatrix[j, i] = matrix1[j, i] + matrix2[j, i];
                }
            }
            return addMatrix;
        }
        /// <summary>
        /// Subtract two matrices
        /// </summary>
        /// <param name="matrix1">First matrix</param>
        /// <param name="matrix2">Second matrix</param>
        /// <returns>The matrix obtained as a result of subtraction</returns>
        public static Matrix Subtract(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1._column != matrix2._column || matrix1._row != matrix2._row)
            {
                throw new ArgumentException("Matrices of different sizes");
            }

            var addMatrix = new Matrix(matrix1._column, matrix1._row);

            for (int i = 0; i < matrix1._row; i++)
            {
                for (int j = 0; j < matrix1._column; j++)
                {
                    addMatrix[j, i] = matrix1[j, i] - matrix2[j, i];
                }
            }
            return addMatrix;
        }
        /// <summary>
        /// Gets the minor from the matrix by the column and row indices
        /// </summary>
        /// <param name="col">Column index from zero</param>
        /// <param name="row">Row index from zero</param>
        /// <returns>Gets the minor from the matrix by the column and row indices</returns>
        public Matrix GetMinor(int col, int row)
        {
            Queue<int> queue = new Queue<int>();
            _lastMinor = new Matrix(_column - 1, _row - 1);

            for (int i = 0; i < _row; i++)
            {
                if (i != row)
                {
                    for (int j = 0; j < _column; j++)
                    {
                        if (j != col)
                        {
                            queue.Enqueue(this[j, i]);
                        }
                    }
                }
            }
            CollectMinor(_row - 1, _column - 1, queue, _lastMinor);
            return _lastMinor;
        }
        /// <summary>
        /// Get the minor K-th order from the matrix by array values
        /// </summary>
        /// <param name="columns">Columns that are in minor</param>
        /// <param name="rows">Rows that are in minor</param>
        /// <returns>Get the minor K-th order from the matrix by array values</returns>
        public Matrix GetMinorKDegree(int[] columns, int[] rows)
        {
            if (columns.Length != rows.Length)
            {
                throw new ArgumentException("The dimensions of the arrays must match");
            }
            Queue<int> queue = new Queue<int>();
            _lastMinorK = new Matrix(columns.Length, columns.Length);

            for (int i = 0; i < _row; i++)
            {
                if (IsValidIndex(i, rows))
                {
                    for (int j = 0; j < _column; j++)
                    {
                        if (IsValidIndex(j, columns))
                        {
                            queue.Enqueue(_matrix[j, i]);
                        }
                    }
                }
            }
            CollectMinor(columns.Length, columns.Length, queue, _lastMinorK);
            return _lastMinorK;
        }

        private void CollectMinor(int row, int col, Queue<int> queue, Matrix minor)
        {
            for (int k = 0; k < row; k++)
            {
                for (int l = 0; l < col; l++)
                {
                    minor[l, k] = queue.Dequeue();
                }
            }
        }

        private bool IsValidIndex(int index, int[] param)
        {
            foreach (var item in param)
            {
                if (index == item)
                {
                    return true;
                }
            }
            return false;
        }
    }
}