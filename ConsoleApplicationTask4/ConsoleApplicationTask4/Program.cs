﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;


namespace ConsoleApplicationTask4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create matrix --- matrix = new Matrix(3, 3)");
            Matrix matrix = new Matrix(3, 3);
            matrix[0, 0] = 1;
            matrix[1, 0] = 2;
            matrix[2, 0] = -1;
            matrix[0, 1] = 1;
            matrix[1, 1] = 0;
            matrix[2, 1] = 3;
            matrix[0, 2] = 7;
            matrix[1, 2] = 8;
            matrix[2, 2] = 4;
            Print(matrix);

            Console.WriteLine("Get minor --- var minor1To1 = matrix.GetMinor(1, 1)");
            var minor1To1 = matrix.GetMinor(1, 1);
            Print(minor1To1);

            Console.WriteLine("Create matrix --- matrix1 = new Matrix(3, 2);");
            Matrix matrix1 = new Matrix(3, 2);
            matrix1[0, 0] = 0;
            matrix1[1, 0] = 1;
            matrix1[2, 0] = 2;
            matrix1[0, 1] = 1;
            matrix1[1, 1] = 2;
            matrix1[2, 1] = 3;

            Console.WriteLine("Get minor --- var minor0To03 = matrix1.GetMinor(2, 0);");
            var minor0To03 = matrix1.GetMinor(2, 0);
            Print(minor0To03);


            Console.WriteLine("Create matrix --- matrix2 = new Matrix(2, 2)");
            Matrix matrix2 = new Matrix(2, 2);

            matrix2[0, 0] = 7;
            matrix2[0, 1] = 5;
            matrix2[1, 0] = 8;
            matrix2[1, 1] = 4;
            Console.WriteLine("Get minor --- var minor3 = matrix2.GetMinor(1, 0)");
            var minor3 = matrix2.GetMinor(1, 0);
            Print(minor3);

            Console.WriteLine("Create matrix --- super = new Matrix(5, 3);");
            Matrix super = new Matrix(5, 3);
            Console.WriteLine("Fil matrix --- super.FillMatrix(3, -2, 4, 1, 0, 1, 0, -2, 3, -1, 3, -4, 14, -7, 3)");
            super.FillMatrix(3, -2, 4, 1, 0, 1, 0, -2, 3, -1, 3, -4, 14, -7, 3);
            Print(super);
            Console.WriteLine("Create minorKDegree --- super.GetMinorKDegree(new[] {1, 4}, new[] {0, 2})");
            var secondDegree = super.GetMinorKDegree(new[] {1, 4}, new[] {0, 2});
            Print(secondDegree);
            Console.WriteLine("Create minorKDegree --- super.GetMinorKDegree(new[] {0, 2, 3}, new[] {0, 1, 2})");
            var thirdDegree = super.GetMinorKDegree(new[] {0, 2, 3}, new[] {0, 1, 2});
            Print(thirdDegree);

            Console.WriteLine("Create matrix --- m3 = new Matrix(2, 3)");
            Console.WriteLine("Create matrix --- m4 = new Matrix(2, 3)");
            Matrix m3 = new Matrix(2, 3);
            Matrix m4 = new Matrix(2, 3);
            Console.WriteLine("Fill matrix --- m3.FillMatrix(4, 2, 9, 0, 4, -6);");
            Console.WriteLine("Fill matrix --- m4.FillMatrix(3, 1, -3, 4, 9, 1);");
            m3.FillMatrix(4, 2, 9, 0, 4, -6);
            m4.FillMatrix(3, 1, -3, 4, 9, 1);
            Console.WriteLine("Add matrix --- Matrix.Add(m3, m4)");
            var sum1 = Matrix.Add(m3, m4);
            Print(sum1);
            Console.WriteLine("Subtract matrix --- Matrix.Add(m3, m4)");
            var sub1 = Matrix.Subtract(m3, m4);
            Print(sub1);

            Console.WriteLine("Create matrix --- mul1 = new Matrix(2, 3)");
            Console.WriteLine("Create matrix --- mul2 = new Matrix(3, 2)");
            Matrix mul1 = new Matrix(2, 3);
            Matrix mul2 = new Matrix(3, 2);
            mul1.FillMatrix(2, 1, -3, 0, 4, -1);
            mul2.FillMatrix(5, -1, 6, -3, 0, 7);
            Print(mul1);
            Print(mul2);
            Console.WriteLine("multiply matrix --- Matrix.Мultiply(mul1, mul2)");
            var multiply = Matrix.Мultiply(mul1, mul2);
            Print(multiply);
            Console.ReadLine();
        }

        private static void Print(Matrix mass)
        {
            for (int i = 0; i < mass.Row; i++)
            {
                for (int j = 0; j < mass.Column; j++)
                {
                    Console.Write(mass[j,i]+" ");
                }
                Console.WriteLine();
            }
        }
    }
}